(function(messages){
	
	"use strict";
	
	 $.widget('kc.appMessagesListener', {
		 
		 _create: function(){
			 
			 for(var msg in messages){
				 var token = PubSub.subscribe(messages[msg], this._log);
			 }

		 },
		 _log: function(msg, data){
			 //console.log(msg, data);
		 }
	 });
	
})(app.messages);