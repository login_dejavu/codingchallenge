﻿(function ($) {

    "use strict";

    var routeModes = [
        {
            mode: 'car',
            title: 'Car',
            icon: '/content/img/noun_project_72.png'
        },
        {
            mode: 'publicTransport',
            title: 'Public transport',
            icon: '/content/img/noun_project_387.png'
        },
        {
            mode: 'pedestrian',
            title: 'Walking',
            icon: '/content/img/noun_project_3516.png'
        }
    ];

    $.widget('here_cc.routeModeList', {

        waypointsLength: 0,

        options: {
            dataKey: 'route-mode',
            defaultRouteMode: 'car',
            uiRouteModeItemTmpl: null,
            uiRouteModeItemSelector: '.btn-route-mode'
        },
        _create: function () {
            this.renderList();
            this._setupEvents();
        },
        _setupEvents: function () {
            var self = this;

            self.element.on('click', self.options.uiRouteModeItemSelector, $.proxy(self.onModeClick, self));

            PubSub.subscribe(app.messages.ROUTE_UPDATED, $.proxy(self.onRouteUpdated, self));
        },
        onRouteUpdated: function (msg, route) {

            if (route && route.length && route.length !== this.waypointsLength) {

                this.waypointsLength = route.length;

                if (this.waypointsLength < 2) {
                    this.element.hide();
                } else {
                    this.element.show();
                }
            }
        },
        onModeClick: function (e) {
            var self = this;
            var uiTarget = $(e.target);

            if (!uiTarget.is(self.options.uiRouteModeItemSelector)) {
                uiTarget = uiTarget.parents(self.options.uiRouteModeItemSelector);
            }

            if (uiTarget.is('.active')) {
                return;
            }

            uiTarget.siblings(self.options.uiRouteModeItemSelector).removeClass('active');
            uiTarget.addClass('active');

            var dataItem = uiTarget.data(self.options.dataKey);            
            PubSub.publish(app.messages.ROUTE_CHANGE_MODE, dataItem.mode);
        },
        renderList: function () {
            var self = this;
            var template = self.options.uiRouteModeItemTmpl;

            var html = [];
            $(routeModes).each(function (i, routeMode) {

                var uiModeItem = $(template).tmpl(routeMode);
                $(uiModeItem).data(self.options.dataKey, routeMode);

                if (routeMode.mode === self.options.defaultRouteMode) {
                    $(uiModeItem).addClass('active');
                }

                html.push(uiModeItem);
            });

            $(html).appendTo(self.element);
        }
    });

})(jQuery);