﻿(function ($) {

    "use strict";

    $.widget('here_cc.hereMap', {

        map: null,
        routeContainer: null,

        options: {
            app_id: "_peU-uCkp-j8ovkzFGNU",
            app_code: "gBoUkAMoxoqIWfxWA5DuMQ",
            defaultLanguage: 'en-US'
        },
        _create: function () {

            nokia.Settings.set("app_id", this.options.app_id);
            nokia.Settings.set("app_code", this.options.app_code);
            nokia.Settings.set("defaultLanguage", this.options.defaultLanguage);

            this.map = new nokia.maps.map.Display(this.element[0], {
                center: [52.51, 13.4],
                zoomLevel: 10,
                components: [
                new nokia.maps.map.component.ZoomBar(),
		        new nokia.maps.map.component.Behavior()
                /*new nokia.maps.map.component.TypeSelector(),*/
                /*new nokia.maps.map.component.Traffic(),*/
                /*new nokia.maps.map.component.PublicTransport(),*/
                /*new nokia.maps.map.component.DistanceMeasurement(),*/
                /*new nokia.maps.map.component.ScaleBar(),*/
                /*new nokia.maps.positioning.component.Positioning(),*/
                /*new nokia.maps.map.component.ContextMenu(),*/
                /*new nokia.maps.map.component.ZoomRectangle()*/
	        ]
            });

            this.resize();
            this._setupEvents();
        },
        _setupEvents: function () {
            var self = this;

            $(window).resize($.proxy(self.resize, self));
            PubSub.subscribe(app.messages.MAP_DRAW_ROUTE, $.proxy(self.onDrawRoute, self));
            PubSub.subscribe(app.messages.MAP_CLEAR_ROUTE, $.proxy(self.onClearRoute, self));
        },
        resize: function () {
            var self = this;
            self.map.setPadding(0, 0, 0, $(window).width() / 2);

            // update bbox
            if (self.routeContainer) {
                setTimeout(function () {
                    self.map.zoomTo(self.routeContainer.getBoundingBox(), false, "default");
                }, 1000)
            }
        },
        onClearRoute: function () {

            if (this.routeContainer) {
                this.map.objects.remove(this.routeContainer);
            }
        },
        onDrawRoute: function (msg, route) {

            this.onClearRoute();

            this.routeContainer = new nokia.maps.routing.component.RouteResultSet(route).container;
            this.map.objects.add(this.routeContainer);
            this.map.zoomTo(this.routeContainer.getBoundingBox(), false, "default");
        }
    });

})(jQuery);