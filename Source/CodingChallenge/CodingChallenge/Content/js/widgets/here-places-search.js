﻿(function ($) {

    "use strict";

    $.widget('here_cc.herePlacesSearch', {
        searchManager: null,

        _create: function () {
            this.searchManager = nokia.places.search.manager;
            this._setupEvents();
        },
        _setupEvents: function () {
            var self = this;

            PubSub.subscribe(app.messages.SEARCH_PLACE_TRIGGER, $.proxy(self.onSearchPlaceTrigger, self));
            PubSub.subscribe(app.messages.SEARCH_REVERSEGEOCODE_TRIGGER, $.proxy(self.onReversegeocodeTrigger, self));
        },
        onReversegeocodeTrigger: function (msg, point) {
            var self = this;

            self.searchManager.reverseGeoCode({
                latitude: point.lat,
                longitude: point.lng,
                onComplete: $.proxy(self.onReversegeocodeCallback, self)
            })
        },
        onReversegeocodeCallback: function (data, status) {
            if (status === 'OK') {
                            
                var searchResult = new app.objects.searchPlaceResult(data.location.position.latitude, data.location.position.longitude);
                searchResult.title = data.name.split(',')[0];
                searchResult.desc = data.name;

                PubSub.publish(app.messages.SEARCH_RESULT_ITEM_SELECTED, searchResult);
            }
        },
        onSearchPlaceTrigger: function (msg, searchQuery) {

            this.searchManager.findPlaces({
                searchTerm: searchQuery,
                onComplete: $.proxy(this.onSearchCompleteCallback, this)
            });
        },
        onSearchCompleteCallback: function (data, status) {
            if (status === "OK") {

                var resultItems = data.results.items.map(function (resultItem) {

                    var item = new app.objects.searchPlaceResult();
                    item.id = resultItem.placeId;
                    item.title = resultItem.title;
                    item.desc = resultItem.vicinity.replace(/<br\/>/g, ', ');

                    if (resultItem.position) {
                        item.lat = resultItem.position.latitude;
                        item.lng = resultItem.position.longitude;
                    }

                    return item;
                });


                if (resultItems.length == 1) {
                    PubSub.publish(app.messages.SEARCH_RESULT_ITEM_SELECTED, resultItems[0]);
                } else {
                    PubSub.publish(app.messages.SEARCH_PLACE_COMPLETED, resultItems);
                }
            }
        }
    });

})(jQuery);