﻿(function ($) {

    "use strict";

    $.widget('here_cc.routeUrlState', {
        options: {
            defaultRouteMode: 'car'
        },
        _create: function () {

            this.parseHashUrl();
            this._setupEvents();
        },
        _setupEvents: function () {
            var self = this;

            PubSub.subscribe(app.messages.ROUTE_UPDATED, $.proxy(self.updateHash, self));
        },
        parseHashUrl: function () {
            var view = hash.get('view');
            var route = hash.get('route');
            var mode = hash.get('mode') || this.options.defaultRouteMode;

            if (view === 'search') {
                route = route.split(',');

                var waypoints = [];
                for (var i = 0; i < route.length; i = i + 2) {

                    var lat = parseFloat(route[i]);
                    var lng = parseFloat(route[i + 1]);

                    var result = new app.objects.searchPlaceResult(lat, lng);
                    PubSub.publish(app.messages.SEARCH_REVERSEGEOCODE_TRIGGER, result);
                    waypoints.push(result);
                }
            }
        },
        updateHash: function (msg, data) {

            if (data.length === 0) {

                if (hash.get('view') === 'search') {
                    hash.remove('search');
                }
                hash.remove('route');
                return;
            }

            var routeHash = [];

            for (var i = 0; i < data.length; i++) {
                routeHash.push(data[i].lat + ',' + data[i].lng);
            }

            hash.add({ view: "search", route: routeHash.join(',') });
        }
    });

})(jQuery);