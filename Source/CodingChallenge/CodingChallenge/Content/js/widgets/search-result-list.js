﻿(function ($) {

    "use strict";

    $.widget('here_cc.searchResultList', {
        options: {
            uiSearchResultItemTmpl: null,
            dataKey: 'search-result-item'
        },
        _create: function () {
            this._setupEvents();
        },
        _setupEvents: function () {
            var self = this;

            self.element.on('click', '.list-group-item', $.proxy(self.onResultClick, self));
            PubSub.subscribe(app.messages.SEARCH_PLACE_COMPLETED, $.proxy(self.onSearchPlaceCompleted, self));
            PubSub.subscribe(app.messages.SEARCH_PLACE_TRIGGER, $.proxy(self.hide, self));
            $(document).click($.proxy(self.hide, self));
        },
        show: function () {
            this.element.show();
        },
        hide: function () {
            this.element.hide();
        },
        onSearchPlaceCompleted: function (msg, results) {
            var self = this;

            self.element[0].innerHTML = '';
            var template = self.options.uiSearchResultItemTmpl;

            var html = [];
            for (var i = 0; i < results.length; i++) {
                var uiResultItem = $(template).tmpl(results[i]);
                uiResultItem.data(self.options.dataKey, results[i]);
                html.push(uiResultItem);
            }

            $(html).appendTo(self.element);

            self.show();
        },
        onResultClick: function (e) {

            var targetEl = $(e.target);
            var el = targetEl.is('.list-group-item') ? targetEl : targetEl.parents('.list-group-item');

            var selectedItem = el.data(this.options.dataKey);

            PubSub.publish(app.messages.SEARCH_RESULT_ITEM_SELECTED, selectedItem);

            this.hide();
        }
    });

})(jQuery);