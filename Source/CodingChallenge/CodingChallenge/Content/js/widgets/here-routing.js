﻿(function ($) {

    "use strict";

    $.widget('here_cc.hereRouting', {
        router: null,
        route: [],

        options: {
            defaultRouteMode: 'car'
        },

        _create: function () {
            this.router = new nokia.maps.routing.Manager();
            this._setupEvents();
        },
        _setupEvents: function () {
            var self = this;

            self.router.addObserver("state", $.proxy(self.onRouteCalculated, self));
            PubSub.subscribe(app.messages.ROUTE_WAYPOINT_ADDED, $.proxy(self.onWaypointAdded, self));
            PubSub.subscribe(app.messages.ROUTE_WAYPOINT_DELETED, $.proxy(self.onWaypointDeleted, self));

            PubSub.subscribe(app.messages.ROUTE_UPDATE_WAYPOINTS, $.proxy(self.onUpdateWaypoints, self));

            PubSub.subscribe(app.messages.ROUTE_CHANGE_MODE, $.proxy(self.onChangeMode, self));
        },
        onChangeMode: function (msg, mode) {
            this.options.defaultRouteMode = mode;
            this.calculateRoute();

            PubSub.publish(app.messages.ROUTE_UPDATED, this.route);
        },
        onWaypointAdded: function (msg, item) {
            this.route.push(item);
            this.calculateRoute();
            PubSub.publish(app.messages.ROUTE_UPDATED, this.route);
        },
        onUpdateWaypoints: function (msg, waypoints) {
            this.route = waypoints;
            this.calculateRoute();
            PubSub.publish(app.messages.ROUTE_UPDATED, this.route);
        },
        calculateRoute: function () {
            var self = this;
            var route = self.route;

            if (route.length < 2) {
                PubSub.publish(app.messages.MAP_CLEAR_ROUTE);
                return;
            }

            // Create waypoints
            var waypoints = new nokia.maps.routing.WaypointParameterList();
            for (var i = 0; i < route.length; i++) {                
                waypoints.addCoordinate(new nokia.maps.geo.Coordinate(route[i].lat, route[i].lng));
            }

            var modes = [{
                type: "fastest",
                transportModes: [self.options.defaultRouteMode],
                trafficMode: "disabled",
                options: ""
            }];

            self.router.calculateRoute(waypoints, modes);
        },
        onWaypointDeleted: function (msg, index) {
            this.route.splice(index, 1);
            this.calculateRoute();
            PubSub.publish(app.messages.ROUTE_UPDATED, this.route);
        },
        onRouteCalculated: function (observedRouter, key, value) {
            if (value == "finished") {

                var routes = observedRouter.getRoutes();
                var route = routes[0];

                PubSub.publish(app.messages.MAP_DRAW_ROUTE, route);
            }
        }
    });

})(jQuery);