﻿(function ($) {

    "use strict";

    $.widget('kc.shareProposal', {
        options: {
            cookieName: 'share_proposal',
            cookieExpiresDays: 30,
            showAfterInterval: 13000
        },
        _create: function () {
            var self = this;

            if (!$.cookie(self.options.cookieName)) {
                window.setTimeout($.proxy(self.showProposal, self), self.options.showAfterInterval);
            }
        },
        showProposal: function () {
            var self = this;

            var template = [
                '<b>{{title}}</b><br/><br/>',
                '<p style="margin-top:4px;font-size:13px;overflow:hidden;"><img style="float:left;margin:2px 10px;max-width:60px;" src="{{thumb}}" /> {{text}}</p>',
            ].join('');

            template = template.replace("{{title}}", "Детский уголок");
            template = template.replace("{{thumb}}", "/content/coloring/1.png");
            template = template.replace("{{text}}", "Если Вам нравится сайт поделитесь пожалуйста ссылкой в социальных сетях. <br/><br/><br/> Спасибо огромное!");

            noty({
                text: template,
                layout: 'bottomLeft',
                type: 'confirm',
                /*timeout: 15000*/
                closeWith: [/*'click',*/ 'button'],
                onClose: function () {

                    $.cookie(self.options.cookieName, 'closed', {
                        expires: self.options.cookieExpiresDays,
                        path: '/'
                    });

                }
            });

        }
    });

})(jQuery);