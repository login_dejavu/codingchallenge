﻿(function ($) {

    "use strict";

    $.widget('here_cc.searchInput', {
        options: {
        },
        _create: function () {
            this._setupEvents();
        },
        _setupEvents: function () {
            var self = this;

            self.element.on('keydown', '.search-query-input', $.proxy(self.onInputKeyDown, self));
            self.element.on('click', '.btn-search-query', $.proxy(self.tirggerSearch, self));
        },
        onInputKeyDown: function (e) {
            if (e.keyCode === 13) {
                this.tirggerSearch();
            }
        },
        tirggerSearch: function () {

            var inputEl = $('.search-query-input', this.element);
            var searchQuery = inputEl.val();

            if (searchQuery !== '') {
                PubSub.publish(app.messages.SEARCH_PLACE_TRIGGER, searchQuery);
            }
        }
    });

})(jQuery);