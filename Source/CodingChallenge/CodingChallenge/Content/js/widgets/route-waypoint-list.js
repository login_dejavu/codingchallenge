﻿(function ($) {

    "use strict";

    $.widget('here_cc.routeWaypointList', {
        options: {
            dataKey: 'route-item',
            newItemHighlightTime: 1500,
            uiRouteItemTmpl: null,
            uiItemSelector: '.list-group-item'
        },
        _create: function () {

            this.element.sortable();
            this.element.disableSelection();

            this._setupEvents();
        },
        _setupEvents: function () {
            var self = this;

            self.element.on('click', '.btn-remove-waypoint', $.proxy(self.onRemoveWaypoint, self));
            self.element.on("sortstop", $.proxy(self.onSortStop, self));
            PubSub.subscribe(app.messages.SEARCH_RESULT_ITEM_SELECTED, $.proxy(self.onSearchResultItemSelected, self));
        },
        onSortStop: function (e, ui) {

            var self = this;
            var waypoints = [];

            $(self.options.uiItemSelector, self.element).each(function (i, uiItem) {
                var wp = $(uiItem).data(self.options.dataKey);
                waypoints.push(wp);
            });

            PubSub.publish(app.messages.ROUTE_UPDATE_WAYPOINTS, waypoints);
        },
        onRemoveWaypoint: function (e) {

            var uiItem = $(e.target).parents(this.options.uiItemSelector);
            var waypointIndex = $(this.options.uiItemSelector, this.element).index(uiItem);

            var item = uiItem.data(this.options.dataKey);
            uiItem.remove();

            PubSub.publish(app.messages.ROUTE_WAYPOINT_DELETED, waypointIndex);
        },
        onSearchResultItemSelected: function (msg, item) {
            var self = this;

            var template = self.options.uiRouteItemTmpl;

            var uiWaypointItem = $(template).tmpl(item);
            uiWaypointItem.data(self.options.dataKey, item);
            uiWaypointItem.appendTo(self.element);
            uiWaypointItem.effect("highlight", {}, self.options.newItemHighlightTime);

            PubSub.publish(app.messages.ROUTE_WAYPOINT_ADDED, item);
        }
    });

})(jQuery);