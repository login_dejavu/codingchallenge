﻿app = window.app || {};

app.share = { url: null };

/*
window.onerror = function (e) {
    console.log(e);
    return true;
}*/

if (!app.debug) {
    window.console = {};
    window.console.log = function () { }
    window.console.time = function () { };
    window.console.timeEnd = function () { };
    window.console.profile = function () { };
    window.console.profileEnd = function () { };
};


app.objects = {};
app.objects.searchPlaceResult = function (lat, lng) {
    this.id = null;
    this.title = null;
    this.desc = null;
    this.lat = lat || 0;
    this.lng = lng || 0;
    this.userInput = false;
}
