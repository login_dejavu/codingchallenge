﻿$(function () {

    var body = $('body');
    var defaultRouteMode = 'car';   

    $('#map').hereMap();
    $('#search').searchInput();

    body.herePlacesSearch();
    body.hereRouting({
        defaultRouteMode: defaultRouteMode
    });    

    $('#search .search-result-list').searchResultList({
        uiSearchResultItemTmpl: '#search-place-result-tmpl'
    });

    $('#search .route-waypoint-list').routeWaypointList({
        uiRouteItemTmpl: '#route-waypoint-tmpl'
    })

    $('#search .route-mode-list').routeModeList({
        defaultRouteMode: defaultRouteMode,
        uiRouteModeItemTmpl: '#route-mode-item-tmpl'
    });

    body.routeUrlState({
        defaultRouteMode: defaultRouteMode
    });

    $('#widgets .widgets-nav a').click(function (e) {

        var anchor = e.target.nodeName.toLowerCase() === "a" ? $(e.target) : $(e.target).parents('a');
        var href = anchor.attr('href');

        $('#widgets-views').toggle();

        anchor.toggleClass('active');
        $(href).toggle();

        e.preventDefault();
    });


});