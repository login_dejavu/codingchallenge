﻿namespace CodingChallenge.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using CodingChallenge.Filters;

    public class HomeController : Controller
    {
        [CompressFilter]
        [Minify]
        public ActionResult Index()
        {
            return View();
        }       
    }
}
