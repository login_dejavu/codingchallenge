﻿namespace CodingChallenge
{
    using System.Web;
    using System.Web.Optimization;

    public class BundleConfig
    {        
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/content/css/main-bundle.css").Include(
                "~/Content/css/bootstrap.css",
                "~/Content/css/bootstrap-theme.css",                
                "~/Content/css/main.css"
            ));            

            var mainJsBundle = new ScriptBundle("~/content/js/main-bundle.js");
#if DEBUG
            mainJsBundle.Include("~/Content/js/debug.js");
#endif

            mainJsBundle.Include(                
                "~/Content/js/libs/jquery.js",
                "~/Content/js/libs/jquery-ui.js",                             
                "~/Content/js/libs/jquery.tmpl.js",                
                "~/Content/js/libs/bootstrap.js",
                "~/content/js/libs/pubsub.js",
                "~/content/js/libs/hash.js",

                "~/Content/js/app.js",                                
                "~/Content/js/app-messages.js",
                
                "~/Content/js/widgets/app-messages-listener.js",
                "~/Content/js/widgets/search-input.js",
                "~/Content/js/widgets/search-result-list.js",
                "~/Content/js/widgets/here-map.js",
                "~/Content/js/widgets/here-places-search.js",
                "~/Content/js/widgets/here-routing.js",                
                "~/Content/js/widgets/route-waypoint-list.js",
                "~/Content/js/widgets/route-mode-list.js",
                "~/Content/js/widgets/route-url-state.js", 
               
                

                "~/Content/js/main-init.js"                
                );           


            bundles.Add(mainJsBundle);            
        }
    }
}